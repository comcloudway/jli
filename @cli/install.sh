echo JLI CLI Installer
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo "executable location (path/to/file.name):"
read -r e

echo Installing Script to: "$e"
touch "$e"
echo "node $SCRIPTPATH/cli.js" '$@' > "$e"
chmod +x "$e"

echo Done
echo You can now use the jli-cli by running "$e" or adding it to your path
