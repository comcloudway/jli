import * as fs from 'fs';
import * as prompt from 'prompt-sync'
import {JLI} from '@comcloudway/jli-core';
import { LISP_COMMANDS as lc, LISP_BLACKLIST as lb, LISP_COMPILER as lcomp, LISP_PROC as lp } from '@comcloudway/jli-lang-lisp';
import { COMMANDMODE_COMMANDS as cc, COMMANDMODE_BLACKLIST as cb, COMMANDMODE_COMPILER as ccomp, COMMANDMODE_PROC as cp } from '@comcloudway/jli-lang-commandmode';

const collective = {
    // LISP
    LISP_COMMANDS: lc,
    LISP_BLACKLIST: lb,
    LISP_COMPILER: lcomp,
    LISP_PROC: lp,

    // COMMANDMODE
    COMMANDMODE_COMMANDS: cc,
    COMMANDMODE_BLACKLIST: cb,
    COMMANDMODE_COMPILER: ccomp,
    COMMANDMODE_PROC: cp
}

const input = prompt.default();

const argumentParser = (args) => {
    let d = {};
    args.forEach(a=>{
        let conf = a.slice(0,2);
        let dat = a.slice(2);
        d[conf]=dat;
    })
    return {
        params: d,
        get: (key, def)=>{
            let r = d[key];
            if(typeof r === "undefined") return def;
            return r;
        }
    }
}

let args = argumentParser(process.argv.slice(1));

let setup = args.get('-s', 'LISP');
let commands = args.get('-e', setup+'_COMMANDS');
let blacklist = args.get('-b', setup+'_BLACKLIST');
let compiler = args.get('-c', setup+'_COMPILER');
let level = args.get('-l', 6)
let proc = args.get('-p', setup+'_PROC');
let state = {};

// initialise JLI with Lisp
let jli = JLI({
    COMMANDS: collective[commands],
    BLACKLIST: collective[blacklist],
    COMPILER: collective[compiler],
    STATE: state,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`${m}`),
        input: (m)=>new Promise ((resolve)=>{
           /* console.log("INPUT: ");
            console.log("no inputprovider found");
              return process.argv[3] || "Sample Input";*/
            let r = input(m+' '||"");
            resolve(r)
        })
    }
});


let file = args.get('-f', undefined);

if(!file) throw new Error("No file specified")

fs.readFile(file, 'utf-8', (err, data)=>{
    if(err){
        console.log("ERROR: " + err);
        return;
    }
    let code = collective[proc](data);
    if(code.startsWith('loop:')) {
        let c = data.split('\n');
        let rowify = (arr) => new Promise((res, rej) => {
            let store = [];
            let add = (data) => {
                state['__last']=data;
                store.push(data);
                if(store.length >= arr.length) {
                    res(store)
                } else {
                    loop()
                }
            }
            let loop = () => {
                let d = arr[store.length];
                if(!d) {
                  add(d);
                } else if(!!d.then) {
                    Promise.resolve(d)
                           .then(da=>add(da))
                           .catch(e=>rej(e))
                } else {
                    let [func, cmd, lv] = d;
                    func(cmd, lv)
                        .then(da=>{
                            add(da)})
                        .catch(e=>rej(e))
                }
            }
            loop();
        });
        rowify(c.map(cmd=>[jli.run, cmd, level]))
            .catch(e=>console.log(e))
    }else {
        jli.run(collective[proc](data), level).catch(e=>console.log(e))
    }
})
