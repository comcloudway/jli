* JLI
[[./icon.png]]
** Table of Contents :toc:
- [[#jli][JLI]]
  - [[#about][About]]
  - [[#roadmap-713-53][Roadmap]]
  - [[#documentation][Documentation]]

** About
#+ATTR_HTML: width="512px"
[[./cover.png]]
JLI(jelly) is a Lisp inspired Array based Programming Language.
** Roadmap [7/13] [53%]
*** TODO Console Output [2/4] [50%]
    - [X] print
    - [ ] format
    - [ ] princ
    - [X] terpri

*** DONE Console Input [1/1] [100%]
    CLOSED: [2020-08-17 Mon 16:00]
    - [X] read

*** DONE MATH [18/18] [100%]
CLOSED: [2021-01-08 Fri 18:55]
    - [X] +
    - [X] -
    - [X] *
    - [X] /
    - [X] rem
    - [X] mod
    - [X] expt
    - [X] sqrt
    - [X] exp
    - [X] log
    - [X] floor
    - [X] ceiling
    - [X] max
    - [X] min
    - [X] oddp
    - [X] evenp
    - [X] numberp
    - [X] null

*** DONE Extras [2/2] [100%]
    CLOSED: [2020-08-17 Mon 16:00]
    - [X] progn
    - [X] let

*** DONE Logical Operators [8/8] [100%]
    CLOSED: [2020-11-21 Sat 21:05]
    - [X] =
    - [X] <=
    - [X] >=
    - [X] <
    - [X] >
    - [X] not
    - [X] and
    - [X] or

*** TODO Equality [2/3] [66%]
    - [X] eq
    - [ ] equal
    - [X] equalp

*** DONE Conditionals [4/4] [100%]
CLOSED: [2021-01-07 Thu 19:06]
    - [X] if
    - [X] cond
    - [X] case
    - [X] when

*** DONE Variables [4/4] [100%]
    CLOSED: [2021-01-07 Thu 19:04]
    - [X] defvar
    - [X] setf
    - [X] setq
    - [X] defparameter

*** DONE DataTypes [2/2] [100%]
    CLOSED: [2020-11-21 Sat 21:05]
    - [X] Symbols
    - [X] Boolean

*** TODO Functions [1/3] [33%]
    - [X] defunc
    - [ ] flet
    - [ ] lambda

*** TODO LOOPS [0/3] [0%]
   - [ ] loop
   - [ ] dotimes
   - [ ] dolist
   - [ ] unless

*** TODO CELLS/LISTS [0/6] [0%]
   - [ ] cons
   - [ ] list
   - [ ] INDEXING [0/7] [0%]
     - [ ] car
     - [ ] cdr
     - [ ] cadr
     - [ ] caddr
     - [ ] cadddr
     - [ ] caddddr
     - [ ] cadaddr
   - [ ] listp
   - [ ] append
   - [ ] push

*** TODO Documentation
** Documentation
*** Installation
JLI is divided into multiple submodules. To install the JLI core run " npm install @comcloudway/jli-core " which automatically installs the JLI Core Language. To install other languages run " npm install @comcloudway/jli-lang-${language} ".
*** Importing
JLI is shipped as a module meaning you can just import it as a module
#+BEGIN_SRC javascript
  // import JLI
  import { JLI } from '@comcloudway/jli-core';

  // import JLI Core Command set and Default Kit
  import { COMMANDS, DEFAULTS } from '@comcloudway/jli-lang-core';
#+END_SRC
JLI ships with a simple lisp integration that can be loaded and initialised:
#+BEGIN_SRC javascript
  // import ASP
  import { JLI } from '@comcloudway/jli-core';

  // import LISP Commands
  import * as LISP from '@comcloudway/jli-lang-lisp';

  // initialise JLI with LISP commands
  const jli = JLI({COMMANDS: LISP.COMMANDS});
#+END_SRC

*** Initialisation
JLI can be initialised by calling the JLI function. 
You are free to provide a custom Command Set and custom defaults, depending on your use case.
#+BEGIN_SRC javascript
  // COMMANDS and DEFAULT are optional
  const jli = JLI({COMMANDS, DEFAULTS});
#+END_SRC
All the following tutorials asume you have JLI initialised as *jli* and imported the lisp library

*** Configuration
**** COMMANDS
JLI_COMMANDS is an object in which every key represents a function name and every value is a function.
Those functions are called with multiple Arguments, although the firts one is always the TOOLS object.
The TOOLS Object bridges all the default values to the function (e.g. TOOLS.print)
#+BEGIN_SRC javascript
  COMMANDS = {
  // basic command syntax
  [command]:(TOOLS, ...values)=> {
  },
  // example printing to the console
  print: ({print}, msg) => print(msg)
  }
#+END_SRC
**** DEFAULTS
JLI_DEFAULTS is an object containing key - value pairs with the key and a function assigned.
See below for more information on the default setup.
#+BEGIN_SRC javascript
  const DEFAULTS = {
      output: console.log,
      input: (msg)=>{
          console.log(msg);
          return "demo input";
      },
      error: (err) => {
          throw new Error(err);
      }
  };
#+END_SRC
**** BLACKLIST
JLI_BLACKLIST provides a way to disable automatic resolution of all part of the command.
This can be done manually by the command using TOOLS.resolve();
The blacklist is an Array of command names.
**** STATE
JLI_STATE provides the global objects used to create variables and functions.
It might be provided manally given that you want to overwrite it from vanilla javascript.
*** RunLevels
The RunLevel number defines, how lisp like the JLI Code is.
RunLevel is far from lisp, whilst RunLevel 4 acctually is pretty similar.
Although all RunLevels except RunLevel 0 require you to use double quotes, whereas RunLevel 0 allows all kind of quotes supported in JS.
**** RunLevel 0: Javascript Array
The original idea behind JLI was to be run inside your vanilla JS code.
Because all RunLevels depend on one another, each of them is accsessible to the end user.
Tu use RunLevel 0, you don't have to set it manually
#+BEGIN_SRC javascript
jli.run([
    "print",
    "Hello World"
]);
#+END_SRC
Each part of the command is a single array item, arrays can be nested, to build nested commands
#+BEGIN_SRC javascript
jli.run([
'print',
['read', 'Whats your name?']
]);
#+END_SRC
Only when running your code directly using runLevel 0, you have access to your Javascript Variables
#+BEGIN_SRC javascript
jli.run([
"print",
window.location.href
]);
#+END_SRC
**** RunLevel 1: Stringified Javascript Array
Basically the same as RunLevel 0 (@0), but you can no longer use javascript variables, as you provide one String containing the stringified Array. This is useful when fetching a network script without wanting to eval() the code fetched.
RunLevel 1 requires you to manually set the RunLevel.
#+BEGIN_SRC javascript
jli.run('["print", "Hello World"]', 1);
jli.run('["print", ["read", "What\'s your name?"]]', 1);
#+END_SRC
**** RunLevel 2: @1 but with rounded Brackets
RunLevel 2 is basically the same as RunLevel1, but you no longer need to write square brackets, but you can use the round ones instead.
This RunLevel is pretty useless, but allows the Interpreter to interpret the code easier
#+BEGIN_SRC javascript
jli.run('("print", "Hello World")', 2);
jli.run('("print", ("read" "What\'s your name?"))', 2);
#+END_SRC
**** RunLevel 3: @2 but without commas [aka Lispy JLI]
As with RunLevel 2, RunLevel 3, allows you to use rounded brackets, but RunLevel 3 automatically adds a comma to seperate the parameters, making it more like lisp.
This RunLevel was the main RunLevel for quite some time, since building a RegExp capable of replacing the quotes neccassary took some time.
As mentioned above RunLevel 3 still requires you to sorround every paramter with double quotes, except numbers (and true and false, although they are not really lispy)
#+BEGIN_SRC javascript
jli.run('("print" "Hello World")', 3);
jli.run('("print" ("read" "What\'s your name?"))', 3);
#+END_SRC
**** RunLevel 4: @3 but without the need for quotes [aka JLI True Lisp]
RunLevel 4 introduces a completly new way of using JLI, as the most part of the JLI code is now the same as in lisp. Every single paramter is now autoquoted, meaning you do not have to sorround the command name and nil in quotes to use them.
#+BEGIN_SRC javascript
jli.run('(print "Hello World")', 4);
jli.run('(print (read "What\'s your name?', 4);
#+END_SRC
The example code above would look like this if it was written in lisp (and would have all the used commands)
#+BEGIN_SRC elisp
(print "Hello World")
(print (read "What's your name?"))
#+END_SRC
*** JLI()
**** .run(code, runLevel=0)
jli.run(code, runLevel) runs the given code, according to the given runLevel, code has to be an Array for RunLevel 0, or a String for RunLevel 1 and above.
As the code is being converted counting the run levels down, the runLevel 4 will still execute the same functions as runLevel 0. That means, that the code you write not only has to be compatible with the current runLevel, but all runLevels below the selected one.
Once the code has been converted successfully jli.run() executes jli.resolve().
**** .resolve(array)
Basically does the same as jli.run(array, 0), but it is one function less, which technically means it is faster, and as jli.run() depends on resolve, no one cares.
*** JLI vs LISP
*** Examples
See demo.js for a growing selection of examples (might be commented out)
*** Use cases
- custom chat bots
- custom scheme based languages
- lispy feeling
- TurtleOS Shell
