import { JLI } from '@comcloudway/jli-core';
import { LISP_COMMANDS as LISP, LISP_BLACKLIST, LISP_COMPILER } from '@comcloudway/jli-lang-lisp';

// initialise JLI with Lisp
let jli = JLI({
    COMMANDS: LISP,
    BLACKLIST: LISP_BLACKLIST,
    COMPILER: LISP_COMPILER,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`OUT: ${m}`),
        input: ()=>"Hello World"
    }
});
/* CODE TYPES
// use JLI
jli.run([
    "print",
    [
        "+",
        1,
        2
    ]
]);

// use JLI in String
jli.exec('["print", "hello"]');

// use lispy JLI in String
jli.exec('("print", ("+",2,2))', true);

// use JLI like lisp
jli.runLisp('("print" ("+" 1 2))');

// statements
jli.run(
    ["print",
     [
    "if",
    [">", 2, 5],
    ["progn",
     ["+", 1, 3]
    ]
     ]
]);
jli.run([
    "print",
    [
        "if",
        ["not",
         true],
        ["+",
         23,
        20]
    ]
]);
*/

/// DIFFRENT RUNLEVELS
/*console.log(jli.run(`(print ; prints hello world
"Hello world")`, 6));*/
//console.log(jli.run('(print "Hello")',4))
//console.log(jli.run('("print" "Hello")',3))
//console.log(jli.run('("print", "Hello")',2))
//console.log(jli.run('["print", "Hello"]',1))
//console.log(jli.run(['print', 'hi'], 0))

const cond = `
;;;; A Example JLI Code
(defvar *num* 9)
(defun sayhi (name) (progn (print (name))))
(cond ((= (*num*) 15) (sayhi "Kevin"))
((> (*num*) 10) (sayhi "Mike"))
(t (sayhi "Kay"))
)
`;

const casedemo = `
;;;; A Example JLI Code
(setq *num* 2)
(case (*num*)
((+ (*num*) 1) (print "Always"))
(9 (print "Huuuuuu"))
(10 (print "Hiiiiii"))
)
`;

const lisphigh = `
;; Toller code
(setq *frage* "Wie heißt du?")
(setq *welcome* "Hallo ")

(defun askname (frage) (read (frage)))
(defun sayhi (name *welcome*) (print (+ (*welcome*) (name))))

(setq *user* (askname (*frage*)))
(sayhi (*user*))
`;

const asyncdemo = `
(print (+ "Hello World" ", wie jehts"))
(print (read))
(read)
`

const advmath = `
(print (expt 2 2))
`

jli.run(`(print (progn ${advmath}))`, 6);
