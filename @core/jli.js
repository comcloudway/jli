//import * as CORE from '@comcloudway/jli-lang-core';

/**
 * CustomCompiler
 * used to build your own code converter
 * */
class CustomCompiler {
    constructor() {
        this.levels=[]
    }
    add(cb=(input, next)=>{next(input)}) {
        this.levels.push(cb)
    }
    run(input, runLevel) {
        let tasks = this.levels.slice(0,runLevel+1);
        let rv = tasks.reverse();
        let index = rv.length-1-runLevel;
        let last = input;

        let loop = () => new Promise(res=>{
            let fc = tasks[index]
            if(!fc) {
                //DONE
                res(last)
            } else {
                fc(last, (out)=>{
                    last=out;
                    index++;
                    res(loop());
                })
            }
        })

        return new Promise((res)=>{
            loop().then(r=>{
                res(r)
            })
        })
    }
    get getRunLevelCount() {
        return this.levels.length
    }
}

let EMPTYCOMPILER = new CustomCompiler();
EMPTYCOMPILER.add();

/**
* JLI
* initilize jli interpreter
***/
function JLI({COMMANDS=[], COMPILER=EMPTYCOMPILER, DEFAULTS={}, BLACKLIST=[], STATE={}}) {

    const state = STATE;

    const TOOLS = (global) => {

        let loopedWait = (input) => new Promise((res, rej)=>{
                let arr = input;
            if (!Array.isArray(arr)) arr=[arr];
            Promise.all(arr).then(async a=>{
                Promise.all(a.map(item=>{
                    if(typeof item === "undefined"){
                       return item;
                    } else
                    if(!!item.then) {
                        return loopedWait([item])[0];
                    } else if (Array.isArray(item)) {
                        return loopedWait(item);
                    } else {
                        return Promise.resolve(item)
                    }
                })).then(j=>{
                    if(!Array.isArray(input)) res(j[0])
                    res(j)})
                       .catch(e=>rej(e))
            })
                   .catch(e=>rej(e))
        });

        let rowify = (arr) => new Promise((res, rej) => {
            let store = [];
            let add = (data) => {
                store.push(data);
                if(store.length >= arr.length) {
                    res(store)
                } else {
                    loop()
                }
            }
            let loop = () => {
                let d = arr[store.length];
                if(typeof d === "undefined") {
                  add(d);
                } else if(!!d.then) {
                    Promise.resolve(d)
                           .then(da=>add(da))
                           .catch(e=>rej(e))
                } else {
                    let [func, [args, state]] = d;
                    func(args, state)
                        .then(da=>{
                            add(da)})
                        .catch(e=>rej(e))
                }
            }
            loop();
        });

        return {
            print: (...c)=>Promise.resolve(DEFAULTS.output(...c)),
            read: (...c)=>Promise.resolve(DEFAULTS.input(...c)),
            throw: (...c)=>Promise.resolve(DEFAULTS.error(...c)),
            run: run,
            resolve: resolve,
            emit: (...c)=>Promise.resolve(DEFAULTS.event(...c)),
            custom: (...c)=>Promise.resolve(DEFAULTS.custom(...c)),

            unpromisify: loopedWait,
            rowify: rowify,
            // STATES & GLOBAL
            getState: (variable) => {
                let res = global[variable]
                return res;
            },
            setState: (variable, value) => {
                return global[variable]=value;
            },
            state: ()=>global
        }};

    const resolve = ( command, global=state ) => new Promise((resv, reject) => {
        let tools = TOOLS(global);
        let upfy = tools.unpromisify;
        let rwfy = tools.rowify;

        upfy(command).then(cmd=>{
            let isFake = false;

            if(!Array.isArray(cmd)) {
                cmd=[cmd];
                isFake=true;
            }

            let task = COMMANDS[cmd[0]];
            if(!task) {
                //unknown command
                if(isFake) {
                    // string
                    resv(cmd[0]);
                } else {
                    // variable
                    let value = tools.getState(cmd[0]);
                    if(typeof value === "function") {
                        // function call
                        let ctx = value();
                        let state = ctx.state();
                        let args = cmd.slice(1);
                        rwfy(args.map(val=>{ //TODO Test this section
                        return [tools.resolve, [val]]}))
                            .then(col=>{
                                   col.forEach((v, i)=>{
                                       state[ctx.args[i]]=v
                                   });
                                   resolve(ctx.cmd, state)
                                       .then(resp=>{
                                           Object.entries(state).forEach((key, value)=>{
                                                if(!!new RegExp('^[*][\\s\\S]*[*]$').exec(key)) {
                                                    TOOLS(global).setState(key, value);
                                                }
                                           });
                                           resv(resp);
                                       })
                                       .catch(e=>reject(e))
                               })
                            .catch(e=>reject(e))

                    } else {
                        resv(value);
                    }
                }
            } else {
                let args = cmd.slice(1);
                // Command found
                if(BLACKLIST.indexOf(cmd[0]) !== -1) {
                    // run command but do not resolve arguments
                    upfy(args)
                        .then(ag=>{
                            tools.resolve(task(TOOLS(global), ...ag))
                                .then(ret=>resv(ret))
                                .catch(e=>reject(e))
                        })
                        .catch(e=>reject(e))
                } else {
                    // resolve command
                    upfy(args)
                        .then(ag=>{
                            rwfy(ag.map(v=>[resolve, [v, global]]))
                                   .then(a=>{
                                       tools.resolve(task(TOOLS(global), ...a))
                                           .then(ret=>resv(ret))
                                           .catch(e=>reject(e))
                                   })
                                   .catch(e=>reject(e))
                        })
                        .catch(e=>reject(e))
                }
            }
        })
                     .catch(e=>reject(e))
    });
    const run = ( code, runLevel=0, global=state) => new Promise((res, reject) => {
        COMPILER.run(code, runLevel)
            .then(code=>resolve(code, global))
            .then(ret=>res(ret))
            .catch(e=>{
                TOOLS(global).throw(e);
                reject(e.toString())
            })
    });


    return {
        run,
        resolve,
    };
}


export {
    JLI,
    CustomCompiler
};
