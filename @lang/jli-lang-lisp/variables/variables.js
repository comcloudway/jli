export const VARIABLES = {
    // VARIABLES
    "defvar": (TOOLS, variable, initial_value) => new Promise(async (resolve, reject) =>{
        if (!TOOLS.getState(await variable)) resolve(TOOLS.setState(await variable, await initial_value));
        reject(TOOLS.throw(`Variable ${variable} is already defined`))
    }),
    "setq": (TOOLS, variable, value) => new Promise(async (resolve, reject) =>{
        resolve(TOOLS.setState(await variable, await value));
    }),
    "setf": (TOOLS, variable, value) => new Promise(async (resolve, reject) =>{
        resolve(TOOLS.setState(await variable, await value));
    }),
    "defparameter": (TOOLS, variable, value) => new Promise(async (resolve, reject) =>{
        resolve(TOOLS.setState(await variable, await value));
    }),
}
