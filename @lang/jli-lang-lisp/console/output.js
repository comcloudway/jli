export const CONSOLE2 = {
    // CONSOLE - OUTPUT
    "print": ( TOOLS, txt ) => new Promise(async (resolve)=>{
        resolve(await TOOLS.print(await txt));
    }),
    "terpri": ( TOOLS ) => {
        TOOLS.print("\n");
    },

}
