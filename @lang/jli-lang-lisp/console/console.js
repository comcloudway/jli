import {CONSOLE2 as output} from './output.js';
import {CONSOLE1 as input} from './input.js';

export const CONSOLE = Object.assign(output, input)
