import {LISP_COMPILER} from './compiler.js'

// LANG
import {CONSOLE} from './console/console.js';
import {FUNCTIONS} from './functions/functions.js';
import {LOGIC} from './logic/logic.js';
import {LOOPS} from './loops/loops.js';
import {MATH} from './math/math.js';
import {VARIABLES} from './variables/variables.js';
import {STRINGS} from './strings/strings.js';
import {NUMBERS} from './numbers/numbers.js';
//import {LISTS} from './lists/lists.js';


const LISP_COMMANDS = Object.assign({
    // EXTRAS
    "progn": ( TOOLS, ...cmds ) => new Promise(async (resolve, reject)=>{
        if( !cmds || cmds.length<1 ) {
            reject(TOOLS.throw("No Arguments"));
        }
        let ret = await TOOLS.rowify(cmds.map(c=>{
        return [TOOLS.run, [c]]}))
        resolve(ret.pop());
    }),
    "let": (TOOLS, variables,  ...cmds) => new Promise(async (resolve, reject)=>{
        cmds = cmds.map(async cmd => await cmd);
        let state = TOOLS.state();
        let s = Object.assign({}, state);
        variables = await variables;
        variables.forEach(([name, value])=>s[name]=value);
        if( !cmds || cmds.length<1 ) {
            reject(TOOLS.throw("No Arguments"));
        }
        let ret = cmds.map((cmd)=>TOOLS.resolve(cmd,s));
        Object.keys(state).forEach(v=>TOOLS.setState(v, s[v]));
        resolve(ret.pop());
    }),


    // STUFF
    "sym": (TOOLS, ...params) => new Promise(async (resolve)=>{
        params=params.map(async v => await v);
        resolve(params);
    }),
    "dd": (TOOLS) => new Promise(async (res, rej) => {
        rej(TOOLS.throw("err"))
    })

}, Object.assign(
    Object.assign(
        Object.assign(CONSOLE, FUNCTIONS),
        Object.assign(STRINGS, NUMBERS)
    ),
    Object.assign(
        Object.assign(LOGIC, LOOPS),
        Object.assign(MATH, VARIABLES)
    )
));

const LISP_BLACKLIST = [
    "if",
    "when",
    "cond",
    "case",
    "let",
    'sym',
    "defun",
    "loop"
];

const LISP_PROC = (data)=>`(progn ${data})` || '(print "Hello World")';

export {
    LISP_COMMANDS as COMMANDS,
    LISP_BLACKLIST as BLACKLIST,
    LISP_COMPILER as COMPILER,
    LISP_PROC as PROC
};
