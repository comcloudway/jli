export const FUNCTIONS = {
    // FUNCTIONS
    "defun": (TOOLS, name, variables,  code) => new Promise(async (resolve, reject) =>{
        variables = await TOOLS.unpromisify(variables);
        code = await code;
        resolve(TOOLS.setState(await name, ()=>{
            return({
                cmd: code,
                args: variables,
                state: ()=>{
                    let state = TOOLS.state();
                    let s = {};
                    variables.forEach(v=>s[v]=state[v]);
                    return s;
                }
            });
        }));
    }),


}
