import { LISP_COMMANDS, LISP_BLACKLIST } from './lisp.js';
import {LISP_COMPILER} from './compiler.js'


const DEFAULT_LISP_CONFIG = () =>{return {
    COMMANDS: LISP_COMMANDS,
    BLACKLIST: LISP_BLACKLIST,
    COMPILER: LISP_COMPILER,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`OUT: ${m}`),
        input: ()=>"Hello World"
    }
}}

const CONSOLE_LISP_CONFIG = ({prompt}) => Object.assign(DEFAULT_LISP_CONFIG(), {
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(m),
        input: (m="INPUT:")=>prompt(`${m} `)
    }
})

const CUSTOM_LISP_CONFIG = (DEFAULTS) => Object.assign(DEFAULT_LISP_CONFIG(), {
    DEFAULTS: DEFAULTS
})

export {
    DEFAULT_LISP_CONFIG,
    CONSOLE_LISP_CONFIG,
    CUSTOM_LISP_CONFIG
}
