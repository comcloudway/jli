import { CustomCompiler } from '@comcloudway/jli-core';

const LISP_COMPILER = new CustomCompiler();
// ARRAY
LISP_COMPILER.add((inp, next)=>{
    next(inp);
});
// STRINGIFIED ARRAY
LISP_COMPILER.add((inp, next)=>{
    next(JSON.parse(inp))
});
// RQC (R)ounded Brackets (Q)uotes (C)ommas
LISP_COMPILER.add((inp, next)=>{
    inp=inp.replace(new RegExp(`([(])(?=([^"]*"[^"]*")*[^"]*$)`,"g"),"[");
    inp=inp.replace(new RegExp(`([)])(?=([^"]*"[^"]*")*[^"]*$)`,"g"),"]");
    next(inp)
});
// RQ (R)ounded Brackets (Q)uotes
LISP_COMPILER.add((inp, next)=>{
     let reg3 = `(\\s)+(?=(?:[^\\'"]*[\\'"][^\\'"]*[\\'"])*[^\\'"]*$)`;
    inp = inp.replace(new RegExp(reg3,"g"), ", ");
    next(inp);
});
// R (R)ounded Brackets
LISP_COMPILER.add((inp, next)=>{
    let reg4 = `(?<word>([^ 0-9()"]|\\n)+)(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(reg4, 'g'), '"$<word>"');
    next(inp);
});
// Extended1 - MultiLine
LISP_COMPILER.add((inp, next)=>{
    let linebreak = `(\n)(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(linebreak, 'g'), '');
    let closingopening = `((?<sym>.)[(])(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(closingopening, 'g'), '$<sym> (');
    next(inp)
});
// Extended2 - Comments
LISP_COMPILER.add((inp, next)=>{
    let singCom = `(?<comment>(;+) ?.*)(?=([^"]*"[^"]*")*[^"]*$)`;
    let multCom = `(?<comment>(#\\|[^|]*)\\|[^|]*(\\|\\|#))(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(singCom, 'g'), '');
    inp = inp.replace(new RegExp(multCom, 'g'), '');
    next(inp)
});

export {
    LISP_COMPILER
}
