export const OPERATORS = {
    // LOGIC
    ">": (TOOLS, ...vals) =>  new Promise(async (resolve, reject)=>{
        let values=[];
        vals=vals.map(async (v) => await v);
        vals.forEach(v=>{
            if (!isNaN(parseFloat(v))) {
                values.push(parseFloat(v));
            }
        });
        if(values.length<2) {
            reject(TOOLS.throw("Not enough Arguments"));
            return false;
        } else {
            let [v1, v2] = values;
            reject((v1>v2)?true:false);
        }
    }),
    "<": (TOOLS, ...vals) => new Promise(async (resolve, reject)=>{
        let values=[];
        vals = vals.map(async (v) => await v);
        vals.forEach(v=>{
            if (!isNaN(parseFloat(v))) {
                values.push(parseFloat(v));
            }
        });
        if(values.length<2) {
            reject(TOOLS.throw("Not enough Arguments"));
        } else {
            let [v1, v2] = values;
            resolve((v1<v2)?true:false);
        }
    }),
    ">=": (TOOLS, ...vals) => new Promise(async (resolve, reject)=>{
        vals = vals.map(async (v) => await v);
        let values=[];
        vals.forEach(v=>{
            if (!isNaN(parseFloat(v))) {
                values.push(parseFloat(v));
            }
        });
        if(values.length<2) {
            reject(TOOLS.throw("Not enough Arguments"));
        } else {
            let [v1, v2] = values;
            resolve((v1>=v2)?true:false);
        }
    }),
    "<=": (TOOLS, ...vals) => new Promise(async (resolve, reject)=>{
        let values=[];
        vals.forEach(v=>{
            if (!isNaN(parseFloat(v))) {
                values.push(parseFloat(v));
            }
        });
        if(values.length<2) {
            reject(TOOLS.throw("Not enough Arguments"));
        } else {
            let [v1, v2] = values;
            resolve((v1<=v2)?true:false);
        }
    }),
    "=": (TOOLS, ...vals) => new Promise(async (resolve, reject)=>{
        let values=[];
        vals = vals.map(async v => await v);
        vals.forEach(v=>{
            if (!isNaN(parseFloat(v))) {
                values.push(parseFloat(v));
            }
        });
        if(values.length<2) {
            reject(TOOLS.throw("Not enough Arguments"));
        } else {
            let [v1, v2] = values;
            resolve((v1===v2)?true:false);
        }
    }),
}
