import {CONDITIONALS} from './conditionals.js';
import {EQUALITY} from './equality.js';
import {OPERATORS} from './operators.js';
import {STATEMENTS} from './statements.js';

export const LOGIC = Object.assign(
    Object.assign(CONDITIONALS, EQUALITY),
    Object.assign(OPERATORS, STATEMENTS));
