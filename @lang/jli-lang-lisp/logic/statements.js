const condy = (cond=false) => {
    let c = true;
    if (typeof cond === "boolean") return cond;
    if(cond==="nil" || (typeof cond === "string" && cond.toLowerCase()!=="t")) c=false;
    return c;
};
export const STATEMENTS = {
    // STATEMENTS
    "if": (TOOLS, cond, task) => new Promise(async (resolve, reject)=>{
        cond=TOOLS.resolve(await cond);
        if(condy(cond)) {
            // true
            resolve(TOOLS.run(await task));
        } else {
            //false
            reject(false);
        }
    }),
    "when": (TOOLS, cond, ...cmds) => new Promise(async (resolve, reject)=>{
        cond=await TOOLS.resolve(await cond);
        cmds = cmds.map(async c => await c);
        if(condy(cond)) {
            // true
            if( !cmds || cmds.length<1 ) {
                reject(TOOLS.throw("No Arguments"));
        }
        let ret = cmds.map((cmd)=>TOOLS.run(cmd));
            resolve(ret.pop());
        } else {
            //false
            return false;
        }
    }),
    "cond": (TOOLS, ...cases) => new Promise(async (resolve, reject)=>{
        cases=cases.map(async c=>await c);
        if (cases.length >= 1) {
            for (let [cond, task] of cases) {
                if(condy(await TOOLS.resolve(cond))) {
                    resolve(TOOLS.run(task));
                }
            }
            return false;
        }
        reject(TOOLS.throw("No conditions specified"));
    }),
    "case": (TOOLS, variable, ...cases) => new Promise(async (resolve, reject)=>{
        variable = await variable;
        cases=cases.map(async c => await c);
         if (cases.length >= 1) {
            for (let [val, task] of cases) {
                if(condy(await TOOLS.resolve(['=', variable, val]))) { //TODO Replace = with equal once implemented
                    resolve(TOOLS.run(task));
                }
            }
            return false;
        }
        reject(TOOLS.throw("No conditions specified"));
    }),

}
