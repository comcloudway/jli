export const EQUALITY = {
    // EQUALITY
    "eq": (TOOLS, var1, var2) => new Promise(async (resolve, reject)=>{
        var1 = await var1;
        var2 = await var2;
        if(!var1.startsWith("'") || var2.startsWith("'")) reject(false);
        resolve(var1 === var2);
    }),
    "equal": ()=>{},
    "equalp": (TOOLS, var1, var2)=> new Promise(async (resolve, reject)=>{
        resolve(var1 === var2);
    }),

}
