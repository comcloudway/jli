const condy = (cond=false) => {
    let c = true;
    if (typeof cond === "boolean") return cond;
    if(cond==="nil" || (typeof cond !== "boolean" && cond.toLowerCase()!=="t")) c=false;
    return c;
};
export const CONDITIONALS = {
    "not": (TOOLS, cond) => new Promise(async (resolve, reject)=>{
        resolve(!condy(await cond));
    }),
    "and": (TOOLS, cond1, cond2) => new Promise(async (resolve, reject)=>{
        resolve(condy(await cond1) && condy(await cond2));
    }),
    "or": (TOOLS, cond1, cond2) => new Promise(async (resolve, reject)=>{
        resolve(condy(await cond1) || condy(await cond2));
    }),

}
