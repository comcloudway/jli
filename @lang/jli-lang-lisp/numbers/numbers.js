export const NUMBERS = {
    'parse-integer': (TOOLS, num) => new Promise(async (res, rej) => {
        num = await num;
        res(parseInt(num));
    }),
    "read-from-string": (TOOLS, num) => new Promise(async res=>{
        num = await num;
        res(parseFloat(num));
    })
}
