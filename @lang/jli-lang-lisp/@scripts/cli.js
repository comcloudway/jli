import * as fs from 'fs';
import * as prompt from 'prompt-sync'
import {JLI} from '../../jli.js';
import { LISP_COMMANDS as LISP, LISP_BLACKLIST } from '../lisp.js';
import {LISP_COMPILER} from '../compiler.js';

const input = prompt.default();

// initialise JLI with Lisp
let jli = JLI({
    COMPILER: LISP_COMPILER,
    COMMANDS: LISP,
    BLACKLIST: LISP_BLACKLIST,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`${m}`),
        input: (m)=>new Promise ((resolve, reject)=>{
           /* console.log("INPUT: ");
            console.log("no inputprovider found");
              return process.argv[3] || "Sample Input";*/
            let r = input(m+' '||"");
            resolve(r)
        })
    }
});

console.log("hi")

let file = process.argv[2];

fs.readFile(file, 'utf-8', (err, data)=>{
    if(err){
        console.log("ERROR: " + err);
        return;
    }
    jli.run(`(progn ${data})` || '(print "Hello World")', 6);
})
