export const LOOP = {
    // LOOPS
    "loop": (TOOLS, ...args) => new Promise(async (resolve, reject) => {
        if(args.length<1) reject(TOOLS.throw("Not enough Arguments"));
        args=await TOOLS.unpromisify(args);
        if(args[0] === "for") {
            // FOR LOOP
            if(args.length<2) reject(TOOLS.throw("Not enough Arguments"));
            if(args[2] === "in" && args[4] === "do") {
                let code = args.slice(7);
                for (let i of args[3]) {
                    // run the code
                }
            } else if(args[2] === "from" && args[4] === "to" && args[6] === "do") {
                let d = args.slice(0, 7);
                let a = await Promise.all(d.map(item=>TOOLS.resolve(item)));
                let code = args.slice(7);
                let res = "";
                for (let i = parseFloat(a[3]); i<=parseFloat(a[5]); i++) {
                    // run code
                    let state = Object.assign(TOOLS.state(), {
                        [a[1]]: i
                    });
                    res = await TOOLS.run(['progn', ...code], 0, state);
                    Object.entries(state).forEach(([key, value])=>{
                        if(!!new RegExp('^[*][\\s\\S]*[*]$').exec(key)) {
                            TOOLS.setState(key, value);
                        }
                    });
                }
                resolve(res);
            }
        } else {
            // WHILE
        }
    })
}
