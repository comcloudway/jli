export const STRINGS = {
    // concat from elisp
    concat: (TOOLS, ...strings) => new Promise(async (res, rej) => {
        res(strings.join(''));
    })
}
