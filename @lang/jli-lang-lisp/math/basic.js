const MATH1 = {
        // MATH
    "+": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 + val2);
    }),
    "-": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 - val2);
    }),
    "*": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 * val2);
    }),
    "/": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 / val2);
    }),
}

export {
    MATH1
}
