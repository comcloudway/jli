const MATH2 = {
    "mod": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 % val2);
    }),
    "rem": ( TOOLS, val1, val2 ) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        if (!isNaN(val1)) val1=parseFloat(val1);
        if (!isNaN(val2)) val2=parseFloat(val2);
        resolve(val1 % val2);
    }),
    "expt": (TOOLS, val1, val2) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.pow(val1, val2));
    }),
    "exp": (TOOLS, exp) => new Promise(async (resolve, reject) =>{
        exp=await exp;
        resolve(Math.pow(Math.E, exp));
    }),
    "log": (TOOLS, val1, val2) => new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.round(Math.log(val1) / Math.log(val2)*100000000000000)/100000000000000);
    }),
    "sqrt": (TOOLS, val) => new Promise(async (resolve, reject) =>{
        val=await val;
        resolve(Math.sqrt(val));
    }),
    "floor": (TOOLS, val1, val2)=> new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.floor(val1, val2));
    }),
    "ceiling": (TOOLS, val1, val2)=> new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.ceil(val1, val2));
    }),
    "max": (TOOLS, val1, val2)=> new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.max(val1, val2));
    }),
    "min": (TOOLS, val1, val2)=> new Promise(async (resolve, reject) =>{
        val1=await val1;
        val2=await val2;
        resolve(Math.min(val1, val2));
    }),
    "oddp": (TOOLS, val)=> new Promise(async (resolve, reject) =>{
        val=await val;
        resolve(val%2!==0);
    }),
    "evenp": (TOOLS, val)=> new Promise(async (resolve, reject) =>{
        val=await val;
        resolve(val%2===0);
    }),
    "numberp": (TOOLS, val)=> new Promise(async (resolve, reject) =>{
        val=await val;
        resolve(typeof val === "number");
    }),
    "null": (TOOLS, val)=> new Promise(async (resolve, reject) =>{
        val=await val;
        resolve(!val)
    }),

}

export {
    MATH2
}
