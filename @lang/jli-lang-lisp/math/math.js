import {MATH1 as basic} from './basic.js';
import {MATH2 as advanced} from './advanced.js';

export const MATH = Object.assign(basic, advanced)
