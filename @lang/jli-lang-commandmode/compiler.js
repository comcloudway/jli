import { CustomCompiler } from '@comcloudway/jli-core';

const COMMANDMODE_COMPILER = new CustomCompiler();
COMMANDMODE_COMPILER.add((inp, next)=>{
    next(inp);
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    next(JSON.parse(inp));
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    let reg3 = `( )+(?=(?:[^\\'"]*[\\'"][^\\'"]*[\\'"])*[^\\'"]*$)`;
    inp = inp.replace(new RegExp(reg3,"g"), ", ");
    inp=`[${inp}]`
    next(inp)
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    let reg3 = `(["][$](?<vr>.*)["])(?=(?:[^\\'"]*[\\'"][^\\'"]*[\\'"])*[^\\'"]*$)`;
    inp = inp.replace(new RegExp(reg3,"g"), '["$<vr>"]');
    next(inp)
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    let reg4 = `(?<word>([^ 0-9()"]|\\n)+)(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(reg4, 'g'), '"$<word>"');
    next(inp);
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    let linebreak = `(\n+)(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(linebreak, 'g'), ',');
/*    let closingopening = `((?<sym>.)[(])(?=([^"]*"[^"]*")*[^"]*$)`;
    inp = inp.replace(new RegExp(closingopening, 'g'), '$<sym> [');*/
    next(inp)
});
export {
    COMMANDMODE_COMPILER
}
