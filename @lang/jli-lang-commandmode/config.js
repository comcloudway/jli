import { COMMANDMODE_COMMANDS, COMMANDMODE_BLACKLIST } from './commandmode.js';
import {COMMANDMODE_COMPILER} from './compiler.js'


const DEFAULT_COMMANDMODE_CONFIG = () =>{return {
    COMMANDS: COMMANDMODE_COMMANDS,
    BLACKLIST: COMMANDMODE_BLACKLIST,
    COMPILER: COMMANDMODE_COMPILER,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`OUT: ${m}`),
        input: ()=>"Hello World"
    }
}}

const CONSOLE_COMMANDMODE_CONFIG = ({prompt}) => Object.assign(DEFAULT_COMMANDMODE_CONFIG(), {
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(m),
        input: (m="INPUT:")=>prompt(`${m} `)
    }
})

const CUSTOM_COMMANDMODE_CONFIG = (DEFAULTS) => Object.assign(DEFAULT_COMMANDMODE_CONFIG(), {
    DEFAULTS: DEFAULTS
})

export {
    DEFAULT_COMMANDMODE_CONFIG,
    CONSOLE_COMMANDMODE_CONFIG,
    CUSTOM_COMMANDMODE_CONFIG
}
