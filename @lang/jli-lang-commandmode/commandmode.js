import {COMMANDMODE_COMPILER} from './compiler.js';

const COMMANDMODE_COMMANDS = {
    "say": (TOOLS, ...text) => new Promise((res)=>{
        res(TOOLS.print(text.join('')));
    }),
    "ask": ( TOOLS, msg ) => new Promise(async (resolve)=>{
        let d = await TOOLS.read(await msg);
        resolve(d);
    }),
    "as": (TOOLS, variable, ...dotask) => new Promise(async (resolve) =>{
         resolve(TOOLS.setState(await TOOLS.resolve(variable), await TOOLS.resolve(dotask)));
    }),
    "set": (TOOLS, variable, value) => new Promise(async (resolve) =>{
        resolve(TOOLS.setState(await variable, await value));
    }),
    "wait": (TOOLS, timeout) => new Promise(res=>{
        setTimeout(()=>res(), timeout);
    })

}

const COMMANDMODE_BLACKLIST = [
    "as"
];

const COMMANDMODE_PROC = (data)=>(`loop:${data}` || 'sayhi "Hello World"');

export {
    COMMANDMODE_COMMANDS as COMMANDS,
    COMMANDMODE_BLACKLIST as BLACKLIST,
    COMMANDMODE_COMPILER as COMPILER,
    COMMANDMODE_PROC as PROC
};
