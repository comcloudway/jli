import { CORE_COMMANDS, CORE_BLACKLIST } from './core.js';
import {CORE_COMPILER} from './compiler.js'


const DEFAULT_CORE_CONFIG = () =>{return {
    COMMANDS: CORE_COMMANDS,
    BLACKLIST: CORE_BLACKLIST,
    COMPILER: CORE_COMPILER,
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(`OUT: ${m}`),
        input: ()=>"Hello World"
    }
}}

const CONSOLE_CORE_CONFIG = ({prompt}) => Object.assign(DEFAULT_CORE_CONFIG(), {
    DEFAULTS: {
        error: (e)=>console.log(`ERROR: ${e}`),
        output: (m)=>console.log(m),
        input: (m="INPUT:")=>prompt(`${m} `)
    }
})

const CUSTOM_CORE_CONFIG = (DEFAULTS) => Object.assign(DEFAULT_CORE_CONFIG(), {
    DEFAULTS: DEFAULTS
})

export {
    DEFAULT_CORE_CONFIG,
    CONSOLE_CORE_CONFIG,
    CUSTOM_CORE_CONFIG
}
