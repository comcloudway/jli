import { CustomCompiler } from '@comcloudway/jli-core';

const COMMANDMODE_COMPILER = new CustomCompiler();
COMMANDMODE_COMPILER.add((inp, next)=>{
    next(inp);
});
COMMANDMODE_COMPILER.add((inp, next)=>{
    next(JSON.parse(inp));
});

export {
    COMMANDMODE_COMPILER as COMPILER
}
