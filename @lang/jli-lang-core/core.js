import {COMPILER} from './compiler.js';

const CORE_COMMANDS = {
    "help": ( TOOLS ) => {
        TOOLS.print("Not loaded any command palette");
    }
};

const CORE_DEFAULTS = {
    output: console.log,
    input: (msg)=>{console.log(msg); return "demo";},
    error: (err)=>{throw new Error(err);},
    event: (e,d)=>{console.log(`Event:${e};\nData: ${d}`);},
    custom: ()=>{}
};

const CORE_BLACKLIST = [];

const CORE_STATE = {};

const CORE_PROC = (data)=>(`loop:${data}` || 'sayhi "Hello World"');

export {
    CORE_COMMANDS as COMMANDS,
    CORE_BLACKLIST as BLACKLIST,
    COMPILER,
    CORE_PROC as PROC,
    CORE_STATE as STATE,
    CORE_DEFAULTS as DEFAULTS
};
