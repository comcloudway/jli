#!/bin/bash
DIR=$(pwd)

function publish() {
    npm version patch
    ncu -u
    npm publish
}

# LANGUAGES
cd $DIR/@lang/jli-lang-commandmode
publish
cd ../jli-lang-core
publish
cd ../jli-lang-lisp
publish

# CORE
cd $DIR/@core
publish

# CLI
cd $DIR/@cli
publish

# DOCS
cd $DIR/@docs
publish
